<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Солнечный трекер</title>
<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<style>
html{
background-color: rgba(0,0,0,0.25);
padding: 5vmin;
display: flex;
justify-content: center;
font-family: 'Roboto', sans-serif;
}
body{
position: absolute;
background-color: white;
box-shadow: 2vmin 2vmin 3vmin 1.5vmin rgba(0, 0, 0, 0.25);
height: 1000px;
font-size: 3vmin;
}
header{
border-bottom: 1px rgba(0,0,0,0.15) solid;
display:flex;
flex-direction: column;
padding: 2vmin 5vmin;
padding-bottom: 4vmin;
font-size: 8vmin;
font-weight: bold;
}
header h4{
margin-top: 0;
font-size: 3vmin;
color: rgba(0, 0, 0, 0.7);
}
footer{
position: absolute;
bottom: 1vmin;
border-top: rgba(0,0,0,0.15) solid 1px;
width: 90%;
}
nav{
border: solid 1px rgba(0, 0, 0, 0.15);
display: flex;
}
.nav-item{
border: solid 1px rgba(0, 0, 0, 0.15);
color: rgba(0, 0, 0, 0.7);
padding: 1em;
}
a{
text-decoration: none;
}
article{
padding: 6vmin;
}
</style>
<body>
<header>
Проект солнечного трекера
<h4>Здесь можно увидеть показания солнечной батареи. </h4>
</header>
<nav>
<a href = "http://suntracker.tk"><div class="nav-item">Главная страница</div></a>
<a href = "http://213.85.168.61/log.txt"><div class="nav-item">Лог событий</div></a>

</nav>
<article>
<?php
echo "";
?>
</article>
<footer>
Всякая информация о создании, и т. п.
</footer>
</body>
</html>
